# How to use this container

```
$ podman run --rm -it --net=host \
    -v /dev/log:/dev/log \
    -v /dev/sgx_enclave:/dev/sgx/enclave \
    -v /dev/sgx_provision:/dev/sgx/provision \
    -v /var/run/aesmd:/var/run/aesmd \
    registry.gitlab.com/enarx/aesmd
```

The above command will run the container interactively (`-it`) on your system.
It will also remove the container image when execution is complete (`--rm`).

During execution, the host directory `/var/run/aesmd` will be mounted into the
container at the location `/var/run/aesmd`. This means that the socket created
by the daemon process at `/var/run/aesmd/aesm.socket` will be visible on the
host at the same path.

Likewise, the `/dev/sgx_enclave` and `/dev/sgx_provison` device node is shared
to the container so that `aesmd` can access SGX with `/dev/sgx/enclave` and
`/dev/sgx/provision`.

You can tweak these arguments as necessary.
